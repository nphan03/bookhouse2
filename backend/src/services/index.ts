import { Application } from '../declarations';
import users from './users/users.service';
import books from './books/books.service';
import orders from './orders/orders.service';
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application) {
  app.configure(users);
  app.configure(books);
  app.configure(orders);
}
