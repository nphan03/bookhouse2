import { Params } from '@feathersjs/feathers';
import { Service, MongooseServiceOptions } from 'feathers-mongoose';
import { Application } from '../../declarations';

interface UserData {
  _id?: string;
  email: string;
  password: string;
}

export class Users extends Service {
  constructor(options: Partial<MongooseServiceOptions>, app: Application) {
    super(options);
  }
create (data: UserData, params?: Params) {
    // This is the information we want from the user signup data
    const { email, password } = data;
    // The complete user
    const userData = {
      email,
      password,
    };
    // Call the original `create` method with existing `params` and new data
    return super.create(userData, params);
  }
}
