// orders-model.ts - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
import { Application } from '../declarations';
import { Model, Mongoose } from 'mongoose';

export default function (app: Application): Model<any> {
  const modelName = 'orders';
  const mongooseClient: Mongoose = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const schema = new Schema({
    firstname: {type: String, require: true},
    lastname: {type: String, require: true},
    email: { type: String, lowercase: true },
    address: {type: String, require: true},
    city: {type: String, require: true},
    province:{type: String, require: true},
    postal: {type:String, require: true},
    subtotal:{type: Number, require: true},
    cart: {type: Array, require: true},
    paymentmethod: {type: String, require: true},
    user: {type:Schema.Types.ObjectId, ref:'users'}
  }, {
    timestamps: true
  });

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    (mongooseClient as any).deleteModel(modelName);
  }
  return mongooseClient.model<any>(modelName, schema);
}
