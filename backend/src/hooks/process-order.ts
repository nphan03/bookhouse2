// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
  return async (context: HookContext): Promise<HookContext> => {
    const { data } = context;
    if(data.cart.length == 0){
      throw new Error ("Empty Cart");
    }
    if(!data.firstname){
      throw new Error ("Missing first name");
    }
    if(!data.lastname){
      throw new Error ("Missing last name");
    }
    if(!data.address){
      throw new Error ("Missing address");
    }
    if(!data.paymentmethod){
      throw new Error ("Missing payment method");
    }
    if(!data.email){
      throw new Error ("Missing email");
    }
    if(!data.city){
      throw new Error ("Missing city");
    }
    if(!data.email){
      throw new Error ("Missing province");
    }
    if(!data.postal){
      throw new Error ("Missing postal");
    }
    return context;
  };
};
