// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';
import nodemailer from 'nodemailer';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
  return async (context: HookContext): Promise<HookContext> => {
    const { result } = context;

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
      host: "smtp.sendgrid.net",
      port: 587,
      secure:false,
      requireTLS:true,
      auth: {
        user:"apikey",
        pass: process.env.SENDGRID_PASSWORD
      }
    });

// send mail with defined transport object
    await transporter.sendMail({
      from: '"Bookhouse" <project@494910.xyz>',
      to: result.email,
      subject: "Order Confirmation",
      text: "Hello " + result.firstname + ", your order id is: " + result._id +". Subtotal is $" + result.subtotal
    });

    return context;
  };
};
