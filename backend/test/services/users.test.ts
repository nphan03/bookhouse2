import { MongoMemoryServer } from 'mongodb-memory-server';
import appFunc from '../../src/appFunc';

describe('\'users\' service', () => {
  let mongoServer;
  let app;

  const userInfo = {
    email: 'sample@mail.com',
    password:'secret'
  }

  beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    process.env.MONGODBURI = await mongoServer.getUri();
    app = appFunc();
  });

  afterAll(async () => {
    await mongoServer.stop();
  });

  it('registered the service', () => {
    const service = app.service('users');
    expect(service).toBeTruthy();
  });

  it('creates user', async () => {
    const user = await app.service('users').create(userInfo);
    expect(user).toBeTruthy();
  });

  it('creates duplicate user', async () => {
    await expect( async () => {
      const user = await app.service('users').create(userInfo);
    }).rejects.toThrow();
  });
});
