import { MongoMemoryServer } from 'mongodb-memory-server';
import appFunc from '../../src/appFunc';

describe('\'orders\' service', () => {
  let mongoServer;
  let app;

  let bookInfo1 = {
      _id: "606f7b83fb645124721ef9a3",
      title: "Alice In Wonderland",
      qty: 3,
      price: 39.99,
  }
  let bookInfo2 = {
    _id: "606f7b83fb645124721ef9a2",
    title: "The Little Prince",
    qty: 1,
    price: 23.99,
  }
  const orderInfo= {
    firstname: "Anne",
    lastname: "Green",
    email: "anneg@mail.com",
    address: "123 Main",
    province: "BC",
    city: "Vancouver",
    cart: [bookInfo1,bookInfo2],
    subtotal: 143.96,
    paymentmethod: "Credit Card"
  }
  beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    process.env.MONGODBURI = await mongoServer.getUri();
    app = appFunc();
  });

  afterAll(async () => {
    await mongoServer.stop();
  });

  it('registered the service', () => {
    const service = app.service('orders');
    expect(service).toBeTruthy();
  });

  it('creates order', async () => {
    const order = await app.service('orders').create(orderInfo);
    expect(order).toBeTruthy();
  });

  //expect order with empty cart to throw error
  it('order with empty cart to throw error', async () => {
    let order_empty_cart = {...orderInfo};
    order_empty_cart.cart = [];
    await expect( async () => {
      const order = await app.service('orders').create(order_empty_cart);
    }).rejects.toThrow("Empty Cart");
  });

  //expect order with empty cart to throw error
  it('order with empty cart to throw error', async () => {
    let order_empty_cart = {...orderInfo};
    order_empty_cart.cart = [];
    await expect( async () => {
      const order = await app.service('orders').create(order_empty_cart);
    }).rejects.toThrow("Empty Cart");
  });

  //expect order without name to throw error
  it('order with no firstname to throw error', async () => {
    let order_nofname={...orderInfo};
    order_nofname.firstname = null;
    await expect( async () => {
      const order = await app.service('orders').create(order_nofname);
    }).rejects.toThrow("Missing first name");
  });
  it('order with no lastname to throw error', async () => {
    let order_nolname = {...orderInfo};
    order_nolname.lastname = null;
    await expect( async () => {
      const order = await app.service('orders').create(order_nolname);
    }).rejects.toThrow("Missing last name");
  });

  it('order with no address to throw error', async () => {
    let order_noaddress = {...orderInfo};
    order_noaddress.address = null;
    await expect( async () => {
      const order = await app.service('orders').create(order_noaddress);
    }).rejects.toThrow("Missing address");
  });
});

