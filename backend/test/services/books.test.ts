import { MongoMemoryServer } from 'mongodb-memory-server';
import appFunc from '../../src/appFunc';

describe('\'books\' service', () => {
  let mongoServer;
  let app;

  const bookInfo = {
    isbn: "1503222683",
    title: "Alice In Wonderland",
    price: 39.99,
    description: "It tells of a young girl named Alice, who falls through a rabbit hole into a subterranean fantasy world populated by peculiar, anthropomorphic creatures.",
    in_stock: 5,
    author: "Lewis Carroll"
  }

  beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    process.env.MONGODBURI = await mongoServer.getUri();
    app = appFunc();
  });

  afterAll(async () => {
    await mongoServer.stop();
  });

  it('registered the service', () => {
    const service = app.service('books');
    expect(service).toBeTruthy();
  });

  it('creates book', async () => {
    const book = await app.service('books').create(bookInfo);
    expect(book.isbn).toBe("1503222683");
    expect(book.title).toBe("Alice In Wonderland");
  });

  it('creates duplicate book', async () => {
    await expect( async () => {
      const book = await app.service('books').create(bookInfo);
    }).rejects.toThrow();
  });
});
