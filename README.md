# Bookhouse

Frontend: https://bookhouse.494910.xyz  
Backend:  https://bookhouse2-llpymsoicq-uw.a.run.app  

Bookhouse allows users to place online orders. All books are listed on the homepage of Bookhouse, along with books' infor. 
User can add books to their shopping cart, and process to check-out page when they’re ready. 
At check-out page, user provides information like address and chooses favorite payment method. 
Once an order has been placed, a confirmation will be sent to user email.

## About

### 1. Buit with

- Frontend: React, Bootstrap  
- Backend:  [Feathers](http://feathersjs.com)  
- Database: mongodb

### 2. Third-party sites

- [SendGrid](https://sendgrid.com/) to send comfirmation email  
- [Google Analytics](https://analytics.google.com) to enable page and event tracking  

## Run locally

### 1. Clone repo
```bash
git clone git@gitlab.com:nphan03/bookhouse2.git
```
### 2. Run Frontend
Set MONGODBURI and SENDGRID_PASSWORD variables
```bash
cd bookhouse2/frontend
yarn install
yarn build
```
### 3. Run Backend
Start a new terminal.
```bash
cd bookhouse2/backend
export MONGODBURI='your mongodb connection string goes here'
export SENDGRID_PASSWORD='your SendGrid password here'
npm install
PORT=8080 npm run dev
```

## Testing

```bash
cd bookhouse2/backend
PORT=8080 npm run jest
```
## Usage

At the homepage, add books that you want to buy, you can check the shopping cart by clicking the link "Cart" on the top-right corner of page. From the shopping cart, click "Checkout" button at the bottom of shopping cart to proceed to check-out page. In order to place an order, you will be asked to sign-in or sign-up with email/password if you're not yet a member. Then, you can fill in the form for order information. You'll receive a confirmation email with your order id and subtotal when successfully place order.
