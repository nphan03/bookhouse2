import React from "react";
import {toggleShoppingCart} from "./client";

import {
    Link
} from "react-router-dom";
class ShoppingCart extends React.Component{
  constructor(props){
    super(props);
    this.state ={};
  }
    componentDidUpdate(prevProps){
    if(prevProps.cart !== this.props.cart){
      this.props.calculateSubtotal();

    }
  }

  render(){
    return <div className="shoppingcart d-none flex-column p-0 bg-white" id="shopping-cart" aria-labelledby="cart-toggle">
        <div className="cart-header flex-column bg-primary text-white px-1">
            <div className="row">
                <div className="col-10 text-center">Cart</div>
                <button className="col btn-danger" aria-label="close cart" onClick={toggleShoppingCart}>X</button>
            </div>
        </div>

        <div className="cart-content flex-column flex-grow-1 px-1">
            <div className="border-bottom border-secondary text-right">Price</div>
            <ul className="cart-books list-unstyled">
                {this.props.cart.map((book, index) =>
                <li className="cart-book py-3" key={index}>
                    <button className="removeBtn p-0 px-2 btn-danger" type="button" aria-label="remove a book" onClick={()=>this.props.removeBook(book)}>X</button>
                    <div className="cart-book-name p-1">{book.title}</div>
                    <input type="number" className={"cart-qty-"+book._id} name="qty" min="1" max="50" onChange={(e)=>this.props.handleQtychange(e,book)} defaultValue={book.qty}></input>
                    <div className="cart-book-price text-right pl-1">${book.price * book.qty}</div>
                </li>)}
            </ul>
            </div>
        <div className="cart-footer flex-column bg-primary text-white text-right px-1">
            <div className="stotal py-2">Subtotal: ${this.props.subtotal}</div>
            <Link to="/checkout"><button aria-label="Proceed to checkout" className="checkoutbtn btn-success text-white">Checkout</button></Link>
        </div>
    </div>;
  }
}

export default ShoppingCart;
