import $ from 'jquery';
let isHidden=true;

export const toggleShoppingCart = () => {
  if(isHidden){
    $(".shoppingcart")
    .addClass("d-flex")
    .removeClass("d-none") ;
    isHidden=false;
  }else{
  $(".shoppingcart")
    .addClass("d-none")
    .removeClass("d-flex") ;
    isHidden=true;
  }
}

export const findIndexof = (arr, object) => {
  for(let i=0; i<arr.length; i++){
    if(arr[i]._id === object._id) {
      return i;
    }
  }
  return -1;
}