import React, { Component } from "react";
import ReactGA from 'react-ga';
import { createBrowserHistory } from 'history';
import {toggleShoppingCart, findIndexof} from "./client";
import ShoppingCart from "./shoppingcart";
import client from "./feathers";
import $ from 'jquery';
// import Admin from "./admin"
import Checkout from "./checkout";

import {
    //BrowserRouter as Router,
    Router,
    Link,
    Switch,
    Route
} from "react-router-dom";

// Analytics
const trackingId = "UA-191688108-2"; 
ReactGA.initialize(trackingId);

const history = createBrowserHistory();

history.listen(location => {
  ReactGA.pageview(location.pathname); // Record a pageview for the given page
});

//Application class
class Application extends Component {
  constructor(props){
    super(props);
    this.state = {
		books:[],
        cart: [],
        subtotal: 0        
    };
    this.setState = this.setState.bind(this);
    this.handleAddtoCart = this.handleAddtoCart.bind(this);
    this.removeBook = this.removeBook.bind(this);
    this.handleQtychange = this.handleQtychange.bind(this);
    this.calculateSubtotal = this.calculateSubtotal.bind(this);
    }

  handleAddtoCart(book){
    let order_qty=Number($(`input[name=qty-${book._id}]`).val());
    let arr = [...this.state.cart];
    const order_book = {
      _id: book._id,
      title: book.title,
      qty: order_qty,
      price: book.price,
    }
    if(arr.length!==0){
        let duplicate=false;
        for(let i=0; i<arr.length; i++){
            if(arr[i]._id === book._id){
                let currentQty=Number(arr[i].qty);
                arr[i].qty=order_qty+ currentQty;
                $(`.shoppingcart .cart-qty-${book._id}`).val(arr[i].qty);
                i=arr.length;
                duplicate=true;
            }
        }
        if(!duplicate){
            arr.push(order_book);
        }
    }else{
        arr.push(order_book);
    } 
    this.setState({cart: arr});
    $(".book-qty").val("1");
  };
  
  removeBook(book){
    let arr = [...this.state.cart];
    const index = findIndexof(arr, book);
    (index !== -1 ) && arr.splice(index,1);
    this.setState({cart: arr});
  };

  handleQtychange(e,book){
    let arr = [...this.state.cart];
    const index = findIndexof(arr, book);
    (index !== -1 ) && (arr[index].qty=Number(e.target.value));
    this.setState({cart: arr})
  };

  calculateSubtotal(){
    let a=0;
    this.state.cart.map(book =>
      a+=book.price*book.qty
    )
    this.setState({subtotal: a.toFixed(2)});
  }

  componentDidMount(){
    const bookService = client.service('books');

    bookService.find().then(result => {
        this.setState({books: result.data});
    });
  }
  
  render(){
    let shoppingcartProps = {
        handleQtychange: this.handleQtychange,
        removeBook: this.removeBook,
        calculateSubtotal: this.calculateSubtotal,
        cart: this.state.cart,
        subtotal: this.state.subtotal
    }

    return(
      <Router history={history}>
          <header className="header d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
            <h3 className="my-0 mr-md-auto font-weight-normal"><Link to="/">BookHouse</Link></h3>
            {/*<Link className="my-2 my-md-0 mr-md-3" to="/admin">Admin</Link>*/}
            <Link className="my-2 my-md-0 mr-md-3" aria-haspopup="true" aria-controls="shopping-cart" id="cart-toggle" onClick={toggleShoppingCart}>Cart</Link>
          </header>
          <main className="mt-0">
            <Switch>
                <Route exact path="/">
                    <div className="books container">
                        <div className="row">
                            {this.state.books.map(book =>
                            <div className="book card col-sm-12 col-md-4 p-0 mb-1" key={book._id}>
                                <div className="row no-gutters h-100 p-1">
                                    <div className="col-4 p-1">
                                        <img className="card-img w-100" src={book.poster} alt={"book cover-"+book.title}></img>
                                    </div>
                                    <div className="col-8 p-1">
                                        <div className="card-body p-0 h-100 d-flex flex-column align-content-between">
                                            <div className="book-title-qty m-0 text-info">{book.title} <span className="text-success">({book.in_stock} in stock)</span></div>
                                            <div className="book-price">${book.price}</div>
                                            <div className="book-summary flex-grow-1 overflow-auto">{book.description}</div>
                                            <div className="order-action d-flex justify-content-between pt-2">
                                                <input type="number" className="book-qty" name={"qty-"+ book._id} min="1" max={book.in_stock} defaultValue='1'></input>
                                                <button type="button" id="addtocartbutton" className="btn btn-success" aria-label="Add a book to cart" onClick={()=>this.handleAddtoCart(book)}>Add to Cart</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )}
                        </div>
                    </div>
                    <ShoppingCart {...shoppingcartProps}/>
                </Route>
                <Route path="/checkout"><Checkout {...shoppingcartProps}/></Route>
          {/*<Route path="/admin">
            <Admin />
    </Route>*/}
            </Switch>
        </main>
      </Router>
    );
  }
}

export default Application;
