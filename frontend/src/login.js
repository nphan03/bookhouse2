import React, { Component } from "react";
class Login extends Component {
  constructor(props){
    super(props);
    this.state = {}
  }
  render(){
    return( 
        <div>
            <div className="main-heading py-5 text-center">
            	<h2>Log in or Sign up</h2>
        	</div>
            <div className="row">
				<div className="col"></div>
                <div className="col-6 align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
                    <form id="login-form" aria-label="Login form">
                        <div className="form-group">
							<label htmlFor="email">Email</label>
                            <input className="form-control" type="email" id="email" name="email" onChange={this.props.handleUserEmailchange} required/>
                            <div className="invalid-feedback">Missing email</div>
                        </div>
                        <div className="form-group">
							<label htmlFor="password">Password</label>
                            <input className="form-control" type="password" id="password" name="password" onChange={this.props.handleUserPasswordchange} required/>
                            <div className="invalid-feedback">Missing password</div>
                        </div>
						<button type="submit" id="login" aria-label="Login button" className="btn btn-primary btn-lg btn-block" onClick={this.props.handleSigninSubmit}> Log in </button>
						<button type="submit" id="signup" aria-label="Sign up then log in button" className="btn btn-primary btn-lg btn-block" onClick={this.props.handleSignupSubmit}> Sign up and log in </button>
                    </form>
                    <div className="loginError text-danger text-center">{this.props.error}</div>
                </div>
				<div className="col"></div>
        	</div>     
        </div>
    );
  }
}

export default Login;