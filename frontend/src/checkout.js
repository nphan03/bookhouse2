import React, { Component } from "react";
// import Admin from "./admin"
import ReactGA from 'react-ga';
import client from "./feathers";
import Login from "./login";
import Loading from "./loading";
import $ from 'jquery';

class Checkout extends Component {
  constructor(props){
    super(props);
    this.state = {
        formclass: '',
        firstname:'',
        lastname:'',
        email: '',
        address: '',
        city: '',
        province: '',
        postal: '',
        paymentmethod: '',
        user: {
            email: '',
            password: ''
        },
        error: ''    
    }
    this.handleAddressChange=this.handleAddressChange.bind(this);
    this.handlePMChange=this.handlePMChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFNChange = this.handleFNChange.bind(this);
    this.handleLNChange = this.handleLNChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleCityChange = this.handleCityChange.bind(this);
    this.handleProvinceChange = this.handleProvinceChange.bind(this);
    this.handlePostalChange = this.handlePostalChange.bind(this);
    this.handleUserEmailchange = this.handleUserEmailchange.bind(this);
    this.handleUserPasswordchange = this.handleUserPasswordchange.bind(this);
    this.handleSigninSubmit = this.handleSigninSubmit.bind(this);
    this.handleSignupSubmit = this.handleSignupSubmit.bind(this);
    this.login = this.login.bind(this);
  }
	
  handleFNChange(env){
    this.setState({firstname: env.target.value});
  }

  handleLNChange(env){
    this.setState({lastname: env.target.value});
  }

  handleProvinceChange(env){
    this.setState({province: env.target.value});
  }

  handlePostalChange(env){
    this.setState({postal: env.target.value});
  }

  handlePMChange(env){
    this.setState({paymentmethod: env.target.value});
  }

  handleEmailChange(env){
    this.setState({email: env.target.value});
  }

  handleAddressChange(env){
    this.setState({address: env.target.value});
  }
  handleCityChange(env){
    this.setState({city: env.target.value});
  }

  handleUserEmailchange(e){
    this.setState({user: {email: e.target.value, password: this.state.user.password}});
  }
  handleUserPasswordchange(e){
    this.setState({user: {password: e.target.value, email: this.state.user.email}});
  }

async login(credentials){
    try {
      if(!credentials) {
        // Try to authenticate using an existing token
        await client.reAuthenticate()
        .then(login => {
            this.setState({ login: login, email: login.user.email});
        }).catch(err=>{
            this.setState({ login: null, loginerror: "You need to log in before going to checkout page"}); 
        });
      } else {
        // Otherwise log in with the `local` strategy using the credentials we got
        await client.authenticate({
          strategy: 'local',
          ...credentials
        }).then(login => {
            this.setState({ login: login, email: login.user.email});
        }).catch(err=>{
            this.setState({ login: null, loginerror: err.message}); 
        });
      }
    } catch(err) {       
        this.setState({ login: null, loginerror: err.message});      
    }
  };

  async handleSigninSubmit(ev){
    ev.preventDefault();
    if($("#login-form #email").val() !== "" &&  $("#login-form #password").val() !== ""){
        await this.login(this.state.user);
    }else{
        this.setState({ loginerror: "Email and Password are required"}); 
    }
  }

  async handleSignupSubmit(ev){
    ev.preventDefault();
    if($("#login-form #email").val() !== "" &&  $("#login-form #password").val() !== ""){
        // First create the user
        await client.service('users').create(this.state.user);
        // If successful log them in
        await this.login(this.state.user);
    }else{
        this.setState({ loginerror: "Email and Password are required"}); 
    }
  }

  handleSubmit(ev) {
    ev.preventDefault();
    if(ev.target.checkValidity()){
      const orders = client.service('orders');
      const order = {
        firstname: this.state.firstname,
        lastname: this.state.lastname,
        email: this.state.email,
        address: this.state.address,
        postal: this.state.postal,
        province:this.state.province,
        city: this.state.city,
        subtotal:this.props.subtotal,
        cart: this.props.cart,
        paymentmethod: this.state.paymentmethod,
        user: this.state.login.user._id
      };
      orders.create(order)
      .then((result) => {
          console.log(result);
          this.setState({message: `${this.state.firstname}, thank you for your order. A confirmation has been sent to your email`});
          ReactGA.event({
              category: "Email",
              action: "Send",
          });
      })
      .catch(result => {
        this.setState({error: result.message, formclass: ''})
      });
    }else{
      this.setState({formclass: "was-validated"});
    }  
  }
	
  componentDidUpdate(prevProps){
      if(prevProps.cart !== this.props.cart){
        this.props.calculateSubtotal();
      }
  }
  
  componentDidMount() {
    this.login();
  }

  render(){
    if(this.state.login === undefined){
        return <Loading />;
    }else if(this.state.login){
        return <div>
          <div className="main-heading py-5 text-center">
            <h2>Order</h2>
          </div>
          <form noValidate id="carform" onSubmit={this.handleSubmit} className={this.state.formclass} aria-label="Place order form">
			    <div className="row formFeilds">
          		<div className="col-md-6">
				<div className="form-group">
                  <label htmlFor="firstname">First Name</label>
                  <input type="text" id="firstname" className="form-control" value={this.state.firstname} onChange={this.handleFNChange} required/>
                  <div className="invalid-feedback">Missing first name</div>
                </div> 
                <div className="form-group">
                  <label htmlFor="lastname">Last Name</label>
                  <input type="text" id="lastname" className="form-control" value={this.state.lastname} onChange={this.handleLNChange} required/>
                  <div className="invalid-feedback">Missing last name</div>
                </div>
                <div className="form-group">
                  <label htmlFor="emailaddress">Email</label>
                  <input type="email" id="emailaddress" className="form-control" value={this.state.login.user.email} required onChange={this.handleEmailChange}/>            
                  <div className="invalid-feedback">Missing email</div>
                </div>
                <div className="form-group">
                  <label htmlFor="postal">Address</label>
                  <input type="text" id="address" value={this.state.address} className="form-control" required onChange={this.handleAddressChange}/>
                  <div className="invalid-feedback">Missing address</div>
                </div>
                <div className="form-group">
                  <label htmlFor="city">City</label>
                  <input id="city" className="form-control" required value={this.state.city} onChange={this.handleCityChange}/>
                  <div className="invalid-feedback">Missing city</div>
                </div>
                <div className="form-group">
                  <label htmlFor="province">Province</label>
                  <select id="province" className="form-control" required value={this.state.province} onChange={this.handleProvinceChange}>
                    <option selected disabled value="">Choose a province</option>
                    <option>BC</option>
                    <option>Alberta</option>
                  </select>
              <div className="invalid-feedback">Missing province</div>
            </div>
            <div className="form-group">
              <label htmlFor="postal">Postal</label>
              <input type="text" id="postal" value={this.state.postal} className="form-control" placeholder="A0A 0A0" pattern="[A-Za-z][0-9][A-Za-z]\s?[0-9][A-Za-z][0-9]" required onChange={this.handlePostalChange}/>
              <div className="invalid-feedback">A valid postal code is required</div>
            </div>
            <div className="form-group">
              <label htmlFor="province">Payment Method</label>
              <select id="province" className="form-control" required value={this.state.paymentmethod} onChange={this.handlePMChange}>
                <option selected disabled value="">Choose a payment method</option>
                <option>Paypal</option>
                <option>Credit card</option>
                <option>Debit card</option>
                <option>Cash on Delivery</option>
              </select>
              <div className="invalid-feedback">Missing payment method</div>
            </div>
				</div>
				<div className="col-md-6 ">
					<div className="cart-header flex-column bg-primary text-white px-1 text-center">Cart</div>
					<div className="border-bottom border-secondary text-right">Price</div>
					<ul className="cart-books list-unstyled">
						{this.props.cart.map((book, index) =>
						    <li className="cart-book py-3" key={index}>
							<button className="removeBtn p-0 px-2 btn-danger" aria-label="remove-book" type="button" onClick={()=>this.props.removeBook(book)}>X</button>
							<div className="cart-book-name p-1">{book.title}</div>
							<input type="number" name="qty" min="1" max="50" onChange={(e)=>this.props.handleQtychange(e,book)} defaultValue={book.qty}></input>
							<div className="cart-book-price text-right pl-1">${book.price * book.qty}</div>
							</li>)}
					</ul>
					<div className="stotal py-2">Subtotal: ${this.props.subtotal}</div> 
      	</div>
			</div>
            <div className="place-order-success bg-success text-white text-center p-10">{this.state.message}</div>
        <div className="place-order-success bg-danger text-white text-center p-10">{this.state.error}</div>		
			  <button className="btn btn-primary btn-lg btn-block" type="submit" aria-label="Place Order Button">Place order</button>
      </form>
		</div>
    }
    let user = {
        user: this.state.user,
        handleUserEmailchange: this.handleUserEmailchange,
        handleUserPasswordchange: this.handleUserPasswordchange,
        handleSigninSubmit: this.handleSigninSubmit,
        handleSignupSubmit: this.handleSignupSubmit,
        login: this.state.login,
        error: this.state.loginerror
    }
    return <Login {...user}/>;
  }
}

export default Checkout;